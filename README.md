Json Movie Database


### Purpose ###

This is an application I built as part as a team in college, which allows a user to create movie information which will be stored in a database and outputed in a Json service, Built a seperate mobile application that works in conjunction with this, built primarily in html and JavaScript

### Technologies ###

* C#
* ServiceStack
* Sqlite
* JSON